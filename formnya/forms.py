from django import forms
from .models import Entry
from django.forms import ModelForm

class MessageForm(forms.ModelForm):
    class Meta:
        model = Entry
        fields = ('nama', 'dosen', 'SKS', 'deskripsi','semester','kelas')