from django.db import models

# Create your models here.
class Entry(models.Model):
    nama = models.CharField(max_length=30)
    dosen = models.CharField(max_length=30)
    SKS = models.CharField(max_length=30)
    deskripsi = models.CharField(max_length=30)
    semester = models.CharField(max_length=30)
    kelas = models.CharField(max_length=30)

    def __str__(self):
        return self.nama

class Deadline(models.Model):
    tugas = models.CharField(max_length=100)
    tugas_date = models.DateField()
    matkul = models.ForeignKey(Entry, on_delete=models.CASCADE)

    class Meta():
        ordering = ['tugas_date']

    def __str__(self):
        return self.tugas