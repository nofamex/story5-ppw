# Generated by Django 3.0.2 on 2020-02-28 04:04

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('formnya', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='entry',
            name='kelas',
            field=models.CharField(default=django.utils.timezone.now, max_length=30),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='entry',
            name='semester',
            field=models.CharField(default=django.utils.timezone.now, max_length=30),
            preserve_default=False,
        ),
    ]
