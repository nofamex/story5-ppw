from django.shortcuts import render, redirect
from .models import Entry
from story6.models import Aktivitas
from .forms import MessageForm

# Create your views here.
def iniform(request):
    form = MessageForm(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        formed = {
            'nama' : request.POST['nama'],
            'dosen' : request.POST['dosen'],
            'SKS' : request.POST['SKS'],
            'deskripsi': request.POST['deskripsi'],
            'semester' : request.POST['semester'],
            'kelas' : request.POST['kelas'],
            'form' : MessageForm()
        }

        new_entry = Entry(nama=formed['nama'], dosen=formed['dosen'], SKS=formed['SKS'], deskripsi=formed['deskripsi'],semester=formed['semester'],kelas=formed['kelas'])
        new_entry.save()
        new_aktiv = Aktivitas(aktivitas=request.POST['kegiatan'])
        new_aktiv.save()
        return render(request, 'form.html', formed)
    else:
        map = {
            'form' : MessageForm()
        }
        return render(request, 'form.html', map)