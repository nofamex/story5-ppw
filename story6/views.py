from django.shortcuts import render
from .models import Aktivitas, Peserta
# Create your views here.
def active(request):
    if request.method == "POST":
        Aktiv = Aktivitas.objects.all()
        namanya = request.POST['anggota-baru']
        peserta = int(request.POST['idnya'])
        aktivitasnya = Aktivitas.objects.get(id=peserta)
        baruan = Peserta(nama=namanya, kegiatan=aktivitasnya)
        baruan.save()
        pesertanya = Peserta.objects.all()
        map = {
            'isinya' : Aktiv,
            'pesertanya' : pesertanya,
        }

        return render(request,'activity.html',map)
    else: 
        Aktiv = Aktivitas.objects.all()
        pesertanya = Peserta.objects.all()

        map = {
            'isinya' : Aktiv,
            'pesertanya' : pesertanya,
        }

        return render(request,'activity.html',map)

def delete(request,id):
    ent = Aktivitas.objects.get(id = id)
    ent.delete()
    entries = Aktivitas.objects.all()
    pesertanya = Peserta.objects.all()
    map = {
        'isinya' : entries,
        'pesertanya':pesertanya,
    }
    return render(request, 'showing.html', map)
