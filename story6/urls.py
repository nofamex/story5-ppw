from django.urls import path
from . import views

app_name = "story6"

urlpatterns = [
    path('', views.active, name='active'),
    path('delete/<id>',views.delete,name='delete')
]