from django.test import TestCase, Client
from .models import Aktivitas, Peserta
from django.urls import reverse

# Create your tests here.


class EntryModelTest(TestCase):
    def test_string_representation(self):
        aktiv = Aktivitas(aktivitas="My entry title")
        self.assertEqual(str(aktiv), aktiv.aktivitas)
        kegiatan = Peserta(nama="My entry title")
        self.assertEqual(str(kegiatan), kegiatan.nama)

    def test_url_exists(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_page_uses_index_template(self):
        response = Client().get('/activity/')
        self.assertTemplateUsed(response, 'activity.html')

    def test_page_uses_index_template_after_post(self):
        new_act = Aktivitas.objects.create(aktivitas="jalan-jalan",id=3)
        new_act.save()
        response = self.client.post('/activity/',{'idnya':3,'anggota-baru':"ajay"})
        self.assertEqual(response.status_code, 200)

    def test_page_delete(self):
        new_act = Aktivitas.objects.create(aktivitas="jalan-jalan",id=3)
        response = self.client.post(reverse('story6:delete', kwargs={'id': new_act.id}))
        self.assertEqual(response.status_code, 200)