from django.urls import path
from . import views

app_name = 'showing'

urlpatterns = [
    path('', views.showing, name='create'),
    path('delete/<id>',views.delete, name='delete'),
    path('click/<id>',views.clicked, name='click')
]