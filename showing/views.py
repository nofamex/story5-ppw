from django.shortcuts import render, redirect
from formnya.models import Entry, Deadline

# Create your views here.
def showing(request):
    entries = Entry.objects.all()
    map = {
        'data' : entries
    }
    return render(request,'showing.html',map)

def delete(request,id):
    ent = Entry.objects.get(id = id)
    ent.delete()
    entries = Entry.objects.all()
    map = {
        'data' : entries,
    }
    return render(request, 'showing.html', map)

def clicked(request,id):
    nama = Entry.objects.get(id=id).nama
    dosen = Entry.objects.get(id=id).dosen
    SKS = Entry.objects.get(id=id).SKS
    deskripsi = Entry.objects.get(id=id).deskripsi
    semester = Entry.objects.get(id=id).semester
    kelas = Entry.objects.get(id=id).kelas

    tugas = Deadline.objects.filter(matkul = id)
    map = {
        'nama' : nama,
        'dosen' : dosen,
        'SKS' : SKS,
        'deskripsi' : deskripsi,
        'semester' : semester,
        'kelas' : kelas,
        'tugas' : tugas
    }

    return render(request,'isinya.html',map)